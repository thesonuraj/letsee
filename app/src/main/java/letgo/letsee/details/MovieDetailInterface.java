package letgo.letsee.details;

import letgo.letsee.home.HomeInterface;
import letgo.letsee.home.recycler.data.MovieData;

/**
 * Created by sonu on 08/01/18.
 */

public interface MovieDetailInterface extends HomeInterface {
    void setData(MovieData movieData);
}
