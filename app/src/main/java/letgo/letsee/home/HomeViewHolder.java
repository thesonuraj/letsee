package letgo.letsee.home;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import letgo.letsee.R;

/**
 * Created by sonu on 06/01/18.
 */

public class HomeViewHolder {

    RecyclerView dataRecyclerView;
    TextView initialPage;
    TextView nextPage;
    TextView nextToNextPage;

    public HomeViewHolder(View view) {
        dataRecyclerView = view.findViewById(R.id.list);
        initialPage = view.findViewById(R.id.initial_page);
        nextPage = view.findViewById(R.id.next_page);
        nextToNextPage = view.findViewById(R.id.next_to_next_page);
    }
}
