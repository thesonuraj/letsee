package letgo.letsee.home.recycler.data;

import android.view.View;

import letgo.letsee.base.BaseRecyclerViewData;
import letgo.letsee.models.config.Images;
import letgo.letsee.models.response.Result;
import letgo.letsee.utils.PreferenceManager;

/**
 * Created by sonu on 06/01/18.
 */

public class MovieData extends BaseRecyclerViewData {

    private int voteCount;

    private int id;

    private boolean video;

    private double voteAverage;

    private String title;

    private double popularity;

    private String posterPath;

    private String originalLanguage;

    private String originalTitle;

    private String backdropPath;

    private boolean adult;

    private String overview;

    private String releaseDate;

    private String voteAverageString;

    private String genres;

    private transient View.OnClickListener onClickListener;


    public MovieData(Result other) {
        this.voteCount = other.getVoteCount();
        this.id = other.getId();
        this.video = other.isVideo();
        this.voteAverage = other.getVoteAverage();
        this.title = other.getTitle();
        this.popularity = other.getPopularity();
        this.posterPath = other.getPosterPath();
        this.originalLanguage = other.getOriginalLanguage();
        this.originalTitle = other.getOriginalTitle();
        this.backdropPath = other.getBackdropPath();
        this.adult = other.isAdult();
        this.overview = other.getOverview();
        this.releaseDate = other.getReleaseDate();
        this.voteAverageString = other.getVoteAverage() + "/10";
        StringBuilder builder = new StringBuilder();
        int dot = 0;
        for (Integer id : other.getGenreIds()) {
            if (dot != 0) {
                builder.append("◉ ");
            }
            builder.append(PreferenceManager.getInstance().getGenre(id));
            builder.append("  ");
            dot++;
        }
        this.genres = builder.toString();
        type = ITEM_TYPE_MOVIE;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public String getVoteCountString() {
        return voteCount + " Votes";
    }

    public int getId() {
        return id;
    }

    public boolean isVideo() {
        return video;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public String getTitle() {
        return title;
    }

    public double getPopularity() {
        return popularity;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getVoteAverageString() {
        return voteAverageString;
    }

    public String getBigImageUrl() {
        return PreferenceManager.getInstance().getOriginalImageUrl(posterPath);
    }

    public String getImageUrl() {
        return PreferenceManager.getInstance().getImageUrl(getBackdropPath(), PreferenceManager.BACKDROP_SIZE, Images.SIZE_L);
    }

    public String getSmallImageUrl() {
        return PreferenceManager.getInstance().getImageUrl(getBackdropPath(), PreferenceManager.BACKDROP_SIZE, Images.SIZE_S);
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public String getGenres() {
        return genres;
    }
}
