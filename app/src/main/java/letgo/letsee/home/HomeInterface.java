package letgo.letsee.home;

import java.util.List;

import letgo.letsee.base.BaseActivityInterface;
import letgo.letsee.base.BaseRecyclerViewData;
import letgo.letsee.home.recycler.data.MovieData;

/**
 * Created by sonu on 06/01/18.
 */

public interface HomeInterface extends BaseActivityInterface {
    void onDataFetched(List<BaseRecyclerViewData> recyclerViewData);

    void onDataLoaded(List<BaseRecyclerViewData> recyclerViewData, int page);

    void onItemClicked(MovieData item);

    void showRVProgress(boolean show);

    void onPageChanged(int page,int totalPage);
}
