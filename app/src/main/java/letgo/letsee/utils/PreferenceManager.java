package letgo.letsee.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

import letgo.letsee.LetSeeApp;
import letgo.letsee.models.config.Genre;
import letgo.letsee.models.config.Images;

/**
 * Created by sonu on 03/01/18.
 */

public class PreferenceManager {
    private static final String GENERE_SAVED = "genere_saved";
    private static SharedPreferences preferences;
    private static PreferenceManager preferenceManager;
    public static final String BASE_URL = "base_url";
    public static final String BACKDROP_SIZE = "backdrop_size";
    public static final String LOGO_SIZE = "logo_size";
    public static final String POSTER_SIZE = "poster_size";

    public static PreferenceManager getInstance() {
        if (preferenceManager == null) {
            preferenceManager = new PreferenceManager();
            preferences = LetSeeApp.getLetSeeApp().getSharedPreferences("application_data", Context.MODE_PRIVATE);
        }
        return preferenceManager;
    }


    public void saveImageConfiguration(Images images) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(BASE_URL, images.getBaseUrl());
        saveSizes(BACKDROP_SIZE, editor, images.getBackdropSizes());
        saveSizes(LOGO_SIZE, editor, images.getLogoSizes());
        saveSizes(POSTER_SIZE, editor, images.getPosterSizes());
        editor.commit();
    }

    public void saveGeneres(List<Genre> genres) {
        SharedPreferences.Editor editor = preferences.edit();
        for (Genre genre : genres) {
            editor.putString("genre" + genre.getId(), genre.getName());
        }
        editor.putBoolean(GENERE_SAVED, true);
        editor.commit();
    }

    public String getGenre(int id) {
        return preferences.getString("genre" + id, "N/A");
    }

    public boolean configurationSaved() {
        return preferences.getString(BASE_URL, null) != null && preferences.getBoolean(GENERE_SAVED, false);
    }

    private void saveSizes(String sizeType, SharedPreferences.Editor editor, List<String> sizes) {
        int sizeCounter = 0;
        for (String size : sizes) {
            editor.putString(sizeType + sizeCounter, size);
            sizeCounter++;
        }
    }

    private String getImageSize(String sizeType, int size) {
        return preferences.getString(sizeType + size, null);
    }

    public String getImageUrl(String path, String imageType, int size) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(preferences.getString(BASE_URL, null));
        stringBuilder.append(getImageSize(imageType, size));
        stringBuilder.append("/");
        stringBuilder.append(path);
        return stringBuilder.toString();
    }

    public String getOriginalImageUrl(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(preferences.getString(BASE_URL, null));
        stringBuilder.append("w500");
        stringBuilder.append("/");
        stringBuilder.append(path);
        return stringBuilder.toString();
    }

}
