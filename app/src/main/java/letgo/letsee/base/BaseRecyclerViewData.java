package letgo.letsee.base;

import java.io.Serializable;

/**
 * Created by sonu on 06/01/18.
 */

public class BaseRecyclerViewData implements Serializable {

    public static final int ITEM_TYPE_MOVIE = 1;
    public static final int ITEM_TYPE_PROGRESS = 2;
    public static final int ITEM_TYPE_PAGE = 3;

    protected int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public BaseRecyclerViewData(){}

    public BaseRecyclerViewData(int type){
        this.type = type;
    }

}
