package letgo.letsee.details;

import android.view.View;
import android.widget.ImageView;

import letgo.letsee.R;

/**
 * Created by sonu on 08/01/18.
 */

public class MovieDetailsViewHolder {
    ImageView imageView;

    public MovieDetailsViewHolder(View view) {
        imageView = view.findViewById(R.id.image);
    }
}
