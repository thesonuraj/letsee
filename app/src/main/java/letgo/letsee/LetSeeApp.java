package letgo.letsee;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by sonu on 05/01/18.
 */

public class LetSeeApp extends Application {

    private static LetSeeApp letSeeApp;

    @Override
    public void onCreate() {
        super.onCreate();
        letSeeApp = this;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static LetSeeApp getLetSeeApp() {
        return letSeeApp;
    }
}
