package letgo.letsee.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import letgo.letsee.R;
import letgo.letsee.base.BaseActivity;
import letgo.letsee.base.BaseRecyclerViewData;
import letgo.letsee.details.MovieDetailActivity;
import letgo.letsee.home.recycler.HomeAdapter;
import letgo.letsee.home.recycler.data.MovieData;
import letgo.letsee.utils.Constants;
import letgo.letsee.utils.FontWrapper;

public class HomeActivity extends BaseActivity implements HomeInterface {

    HomeViewHolder homeViewHolder;
    HomePresenter homePresenter;
    HomeAdapter homeAdapter;
    private LinearLayoutManager dataLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        homePresenter = new HomePresenter(this);
        init(homePresenter, findViewById(R.id.root), findViewById(R.id.root));
        homeViewHolder = new HomeViewHolder(findViewById(R.id.root));
        homePresenter.onStart(getIntent().getExtras());

        setupRecyclerView();
    }

    private void setupRecyclerView() {
        homeAdapter = new HomeAdapter(this);
        dataLayoutManager = new LinearLayoutManager(this);
        homeViewHolder.dataRecyclerView.setLayoutManager(dataLayoutManager);
        homeViewHolder.dataRecyclerView.setAdapter(homeAdapter);
        homeViewHolder.dataRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItem = dataLayoutManager.getItemCount();
                int getLastVisibleItem = dataLayoutManager.findLastVisibleItemPosition();

                if (getLastVisibleItem == totalItem - 1 && homePresenter.shouldLoadMore()) {
                    homePresenter.loadMore();
                }

            }
        });

    }

    @Override
    public void onDataFetched(List<BaseRecyclerViewData> recyclerViewData) {
        homeAdapter.setData(recyclerViewData);
    }


    @Override
    public void onDataLoaded(List<BaseRecyclerViewData> recyclerViewData, int page) {
        homeAdapter.addDataList(recyclerViewData);
    }

    @Override
    public void onItemClicked(MovieData item) {
        Intent intent = new Intent(this, MovieDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.DATA, item);
        intent.putExtras(bundle);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

    }


    @Override
    public void showRVProgress(boolean show) {
        if (show) {
            BaseRecyclerViewData baseRecyclerViewData = new BaseRecyclerViewData(BaseRecyclerViewData.ITEM_TYPE_PROGRESS);
            if (homeAdapter.getCurrentDataset().get(homeAdapter.getCurrentDataset().size() - 1).getType() != BaseRecyclerViewData.ITEM_TYPE_PROGRESS) {
                homeAdapter.addSingleData(baseRecyclerViewData);
            }
        } else {
            homeAdapter.removeItem(homeAdapter.getCurrentDataset().size() - 1);
        }


    }

    @Override
    public void onPageChanged(int page, int totalPage) {
        if (totalPage > 0) {
            if (page < totalPage - 3) {
                homeViewHolder.initialPage.setText(String.valueOf(page));
                homeViewHolder.nextPage.setText(String.valueOf(page + 1));
                homeViewHolder.nextToNextPage.setText(String.valueOf(page + 2));
            } else {
                homeViewHolder.initialPage.setText(String.valueOf(page - 2));
                homeViewHolder.nextPage.setText(String.valueOf(page - 1));
                homeViewHolder.nextToNextPage.setText(String.valueOf(page));
            }
        }
        makeSelection(page);


    }

    private void makeSelection(int page) {
        if (homeViewHolder.initialPage.getText().toString().equalsIgnoreCase("" + page)) {
            homeViewHolder.initialPage.setTypeface(FontWrapper.getTypeface(this, FontWrapper.Fonts.Bold));
        } else {
            homeViewHolder.initialPage.setTypeface(FontWrapper.getTypeface(this, FontWrapper.Fonts.Regular));

        }
        if (homeViewHolder.nextPage.getText().toString().equalsIgnoreCase("" + page)) {
            homeViewHolder.nextPage.setTypeface(FontWrapper.getTypeface(this, FontWrapper.Fonts.Bold));
        } else {
            homeViewHolder.nextPage.setTypeface(FontWrapper.getTypeface(this, FontWrapper.Fonts.Regular));

        }
        if (homeViewHolder.nextToNextPage.getText().toString().equalsIgnoreCase("" + page)) {
            homeViewHolder.nextToNextPage.setTypeface(FontWrapper.getTypeface(this, FontWrapper.Fonts.Bold));
        } else {
            homeViewHolder.nextToNextPage.setTypeface(FontWrapper.getTypeface(this, FontWrapper.Fonts.Regular));

        }
    }

    public void onPageClicked(View view) {
        TextView pageTextView = (TextView) view;
        int page = Integer.parseInt(pageTextView.getText().toString());
        homePresenter.loadPage(page);
    }

    public void onArrowClicked(View view) {
        switch (view.getId()) {
            case R.id.next: {
                homePresenter.fetchNext();
                break;
            }
            case R.id.first: {
                homePresenter.fetchFirst();
                break;
            }
            case R.id.previous: {
                homePresenter.fetchPrevious();
                break;
            }
            case R.id.last: {
                homePresenter.fetchLast();
                break;
            }
        }
    }
}
