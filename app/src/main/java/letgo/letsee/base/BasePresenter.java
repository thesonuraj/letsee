package letgo.letsee.base;

import android.os.Bundle;

/**
 * Created by sonu on 06/01/18.
 */

public interface BasePresenter {
    void onStart(Bundle bundle);
    void onResume();
    void onDestroy();
    void onPause();
}
