package letgo.letsee.base;

import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

import letgo.letsee.listeners.RetryListener;

/**
 * Created by sonu on 06/01/18.
 */
public class BaseActivity extends AppCompatActivity implements BaseActivityInterface {

    BaseViewHolder baseViewHolder;
    BasePresenter basePresenter;
    View root;


    protected void init(BasePresenter basePresenter, View noContent, View root) {
        baseViewHolder = new BaseViewHolder(noContent);
        this.basePresenter = basePresenter;
        this.root = root;
    }

    @Override
    public void showProgress(boolean show) {
        baseViewHolder.progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        root.setAlpha(show ? 0.5f : 1.0f);
        baseViewHolder.noContentView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showNoContent(boolean show) {
        baseViewHolder.progressBar.setVisibility(show ? View.GONE : baseViewHolder.progressBar.getVisibility());
        baseViewHolder.noContentView.setVisibility(show ? View.VISIBLE : View.GONE);
        baseViewHolder.retry.setEnabled(true);
    }


    @Override
    public void onFailure(String msg, final RetryListener retryListener) {
        if (!TextUtils.isEmpty(msg)) {
            baseViewHolder.wrongText.setText(msg);
        }
        if (retryListener != null) {
            baseViewHolder.retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setEnabled(false);
                    retryListener.onRetryClicked();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (basePresenter != null) {
            basePresenter.onResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        basePresenter.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        basePresenter.onPause();
    }
}
