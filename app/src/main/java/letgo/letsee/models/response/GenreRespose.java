package letgo.letsee.models.response;

import java.util.List;

import letgo.letsee.models.config.Genre;

/**
 * Created by sonu on 12/01/18.
 */

public class GenreRespose {
    List<Genre> genres;

    public List<Genre> getGenres() {
        return genres;
    }
}
