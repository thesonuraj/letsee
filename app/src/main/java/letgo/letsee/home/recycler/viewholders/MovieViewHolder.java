package letgo.letsee.home.recycler.viewholders;

import android.support.v7.widget.RecyclerView;

import letgo.letsee.BR;
import letgo.letsee.databinding.ItemMovieBinding;
import letgo.letsee.home.recycler.data.MovieData;

/**
 * Created by sonu on 06/01/18.
 */

public class MovieViewHolder extends RecyclerView.ViewHolder {
    private final ItemMovieBinding viewDataBinding;

    public MovieViewHolder(ItemMovieBinding viewDataBinding) {
        super(viewDataBinding.getRoot());
        this.viewDataBinding = viewDataBinding;
    }

    public void bind(MovieData item) {
        viewDataBinding.setVariable(BR.data, item);
        viewDataBinding.executePendingBindings();
    }
}
