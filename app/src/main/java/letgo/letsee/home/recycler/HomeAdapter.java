package letgo.letsee.home.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import letgo.letsee.base.BaseAdapter;
import letgo.letsee.base.BaseRecyclerViewData;
import letgo.letsee.databinding.ItemMovieBinding;
import letgo.letsee.home.HomeInterface;
import letgo.letsee.home.recycler.data.MovieData;
import letgo.letsee.home.recycler.viewholders.MovieViewHolder;
import letgo.letsee.home.recycler.viewholders.ProgressViewHolder;

/**
 * Created by sonu on 06/01/18.
 */

public class HomeAdapter extends BaseAdapter {
    HomeInterface homeInterface;

    public HomeAdapter(HomeInterface homeInterface) {
        this.homeInterface = homeInterface;
    }

    @Override
    public RecyclerView.ViewHolder getViewHolderByType(ViewGroup parent, int viewType) {
        if (viewType == BaseRecyclerViewData.ITEM_TYPE_MOVIE) {
            LayoutInflater layoutInflater =
                    LayoutInflater.from(parent.getContext());
            ItemMovieBinding itemBinding =
                    ItemMovieBinding.inflate(layoutInflater, parent, false);
            return new MovieViewHolder(itemBinding);
        } else {
            ProgressBar progressBar = new ProgressBar(parent.getContext());
            RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            progressBar.setLayoutParams(layoutParams);
            return new ProgressViewHolder(progressBar);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieViewHolder) {
            final MovieData item = (MovieData) getItemAtPosition(position);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    homeInterface.onItemClicked(item);
                }
            });
            ((MovieViewHolder) holder).bind(item);
        }
    }

}
