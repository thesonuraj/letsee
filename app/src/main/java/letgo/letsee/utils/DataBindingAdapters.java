package letgo.letsee.utils;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import letgo.letsee.R;

/**
 * Created by sonu on 11/01/18.
 */

public class DataBindingAdapters {

    @BindingAdapter("img_src")
    public static void setImageSource(ImageView imageView, String url) {
        Picasso.with(imageView.getContext()).load(url).placeholder(R.drawable.placeholder_movie_medium).into(imageView);
    }

    @BindingAdapter("img_large_src")
    public static void setLargeImageSource(ImageView imageView, String url) {
        Picasso.with(imageView.getContext()).load(url).placeholder(R.drawable.placeholder_movie).into(imageView);
    }

    @BindingAdapter("img_small_src")
    public static void setSmallImageSource(ImageView imageView, String url) {
        Picasso.with(imageView.getContext()).load(url).placeholder(R.drawable.placeholder_movie_small).into(imageView);
    }
}
