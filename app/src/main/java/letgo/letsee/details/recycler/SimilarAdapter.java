package letgo.letsee.details.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import letgo.letsee.base.BaseAdapter;
import letgo.letsee.base.BaseRecyclerViewData;
import letgo.letsee.databinding.ItemMovieSmallBinding;
import letgo.letsee.details.MovieDetailInterface;
import letgo.letsee.home.recycler.data.MovieData;
import letgo.letsee.home.recycler.viewholders.ProgressViewHolder;

/**
 * Created by sonu on 11/01/18.
 */

public class SimilarAdapter extends BaseAdapter {
    MovieDetailInterface movieDetailInterface;

    public SimilarAdapter(MovieDetailInterface movieDetailInterface) {
        this.movieDetailInterface = movieDetailInterface;
    }

    @Override
    public RecyclerView.ViewHolder getViewHolderByType(ViewGroup parent, int viewType) {
        if (viewType == BaseRecyclerViewData.ITEM_TYPE_MOVIE) {
            LayoutInflater layoutInflater =
                    LayoutInflater.from(parent.getContext());
            ItemMovieSmallBinding itemBinding =
                    ItemMovieSmallBinding.inflate(layoutInflater, parent, false);
            return new SimilarViewHolder(itemBinding);
        } else {
            ProgressBar progressBar = new ProgressBar(parent.getContext());
            RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            progressBar.setLayoutParams(layoutParams);
            return new ProgressViewHolder(progressBar);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SimilarViewHolder) {
            final MovieData item = (MovieData) getItemAtPosition(position);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    movieDetailInterface.onItemClicked(item);
                }
            });
            ((SimilarViewHolder) holder).bind(item);
        }
    }
}
