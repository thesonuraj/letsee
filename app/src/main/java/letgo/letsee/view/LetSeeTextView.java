package letgo.letsee.view;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import letgo.letsee.utils.FontWrapper;

/**
 * Created by sonu on 10/01/18.
 */

public class LetSeeTextView extends AppCompatTextView {
    private FontWrapper.Fonts mTypeFace = FontWrapper.Fonts.Regular;

    public LetSeeTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setTypeface(FontWrapper.getTypeface(getContext(), mTypeFace));
    }

    public LetSeeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LetSeeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
}
