package letgo.letsee.base;

import letgo.letsee.listeners.RetryListener;

/**
 * Created by sonu on 02/01/18.
 */

public interface BaseActivityInterface {
    void showProgress(boolean show);

    void showNoContent(boolean show);

    void onFailure(String msg, RetryListener retryListener);
}
