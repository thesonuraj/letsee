package letgo.letsee.details.recycler;

import android.support.v7.widget.RecyclerView;

import letgo.letsee.BR;
import letgo.letsee.databinding.ItemMovieSmallBinding;
import letgo.letsee.home.recycler.data.MovieData;

/**
 * Created by sonu on 11/01/18.
 */

public class SimilarViewHolder extends RecyclerView.ViewHolder {
    ItemMovieSmallBinding itemMovieSmallBinding;

    public SimilarViewHolder(ItemMovieSmallBinding itemMovieSmallBinding) {
        super(itemMovieSmallBinding.getRoot());
        this.itemMovieSmallBinding = itemMovieSmallBinding;
    }

    public void bind(MovieData item) {
        itemMovieSmallBinding.setVariable(BR.data, item);
        itemMovieSmallBinding.executePendingBindings();
    }
}
