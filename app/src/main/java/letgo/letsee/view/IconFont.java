package letgo.letsee.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import letgo.letsee.R;
import letgo.letsee.utils.FontWrapper;

public class IconFont extends android.support.v7.widget.AppCompatTextView {

    private FontWrapper.Fonts mTypeFace = FontWrapper.Fonts.IconFont;
    private boolean mFeedbackEnabled;
    private boolean mShadowEnabled;
    private int mCurrentColor;

    public IconFont(Context context) {
        super(context);
        if (!isInEditMode())
            initialiseBigText();
    }

    public IconFont(Context context, AttributeSet attr) {
        super(context, attr);
        if (!isInEditMode()) {
            getStuffFromXML(attr);
            initialiseBigText();
        }
    }

    public IconFont(Context context, AttributeSet attr, int i) {
        super(context, attr, i);
        if (!isInEditMode()) {
            getStuffFromXML(attr);
            initialiseBigText();
        }
    }



    private void getStuffFromXML(AttributeSet attr) {
        TypedArray a = getContext().obtainStyledAttributes(attr, R.styleable.IconFont);
        mFeedbackEnabled = a.getBoolean(R.styleable.IconFont_iconfont_enable_feedback, false);
        mShadowEnabled = a.getBoolean(R.styleable.IconFont_iconfont_enable_shadow, false);
        a.recycle();
    }

    private void initialiseBigText() {
        try {
            setTypeface(FontWrapper.getTypeface(getContext(), mTypeFace));
        } catch (Throwable e) {
        }
        setPaintFlags(getPaintFlags() | Paint.ANTI_ALIAS_FLAG | Paint.HINTING_ON);
        mCurrentColor = getCurrentTextColor();
        setFeedbackOnTouch();
        setShadowOnIcon();
    }

    private FontWrapper.Fonts getTypeFace() {
        return mTypeFace;
    }


    public void revealInformation(TextView textViewToDisplay) {//Utils.revealInformation(getContext(), textViewToDisplay, this, getTypeFace());
    }

    /**
     * If flag is true this function enables feedback if text is clicked by lightening the color of the text
     *
     * @param flag
     */
    public void enableFeedbackOnTouch(boolean flag) {
        mFeedbackEnabled = flag;
        setFeedbackOnTouch();
    }

    private void setShadowOnIcon(){
        try {
            if(mShadowEnabled){
                setShadowLayer(2, 1, 1, getResources().getColor(R.color.text_shadow));
            }else{
                setShadowLayer(2, 1, 1, getResources().getColor(R.color.color_transparent));
            }
        } catch (Exception e) {
        }
    }

    public void setShadowOnIconfont(boolean shadowOnIconfont){
        this.mShadowEnabled = shadowOnIconfont;
        setShadowOnIcon();
    }

    public boolean getShadowOnIconFont(){
        return mShadowEnabled;
    }

    private void setFeedbackOnTouch() {

        try {
            if (mFeedbackEnabled) {

                setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (v == null) return true;
                        switch (event.getAction()) {

                            case MotionEvent.ACTION_DOWN:

                                setTextColor(FontWrapper.darkenColor(mCurrentColor, 1f, .7f));
                                break;
                            case MotionEvent.ACTION_UP:

                                setTextColor(mCurrentColor);
                                v.performClick();
                                break;
                            case MotionEvent.ACTION_MOVE:
                            case MotionEvent.ACTION_CANCEL:
                                setTextColor(mCurrentColor);
                        }
                        return true;
                    }
                });
            }
        } catch (Exception e) {
        }
    }

}

