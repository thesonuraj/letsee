package letgo.letsee.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

public class FontWrapper {

    public enum Fonts {
        Bold, Semibold, Regular, Thin, IconFont, Test
    }

    public static String getFontsPath(Fonts font) {

        if (font == Fonts.Bold) {
            return "fonts/ProximaNovaBold.ttf";
        } else if (font == Fonts.Regular) {
            return "fonts/ProximaNovaRegular.ttf";
        } else if (font == Fonts.Semibold) {
            return "fonts/ProximaNova-Semibold.ttf";
        } else if (font == Fonts.Thin) {
            return "fonts/ProximaNovaThin.ttf";
        } else if (font == Fonts.IconFont) {
            return "fonts/iconfont.ttf";
        }

        // regular is default
        return "fonts/ProximaNovaRegular.ttf";
    }

    public static final Hashtable<Fonts, Typeface> typefaces = new Hashtable<Fonts, Typeface>();

    public static Typeface getTypeface(Context c, Fonts name) {

        synchronized (typefaces) {

            if (!typefaces.containsKey(name)) {

                try {
                    String fontPath = getFontsPath(name);
                    if (!TextUtils.isEmpty(fontPath)) {
                        InputStream inputStream = c.getAssets().open(fontPath);
                        File file = createFileFromInputStream(inputStream);
                        if (file == null) {
                            return Typeface.DEFAULT;
                        }
                        Typeface t = Typeface.createFromFile(file);
                        typefaces.put(name, t);
                    } else {
                        return Typeface.DEFAULT;
                    }

                } catch (Throwable e) {
                    return Typeface.DEFAULT;
                }
            }
            return typefaces.get(name);
        }
    }
    public static int darkenColor(int color, float multiplier, float alpha) {
        float[] hsv = new float[3];

        Color.colorToHSV(color, hsv);
        hsv[2] *= multiplier; // value component
        return Color.HSVToColor((int) (255 * alpha), hsv);
    }

    private static File createFileFromInputStream(InputStream inputStream) {

        try {
            File f = File.createTempFile("font", null);
            OutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            outputStream.close();
            inputStream.close();
            return f;
        } catch (Exception e) {
        }

        return null;
    }


}
