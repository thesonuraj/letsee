package letgo.letsee.base;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonu on 06/01/18.
 */

public abstract class BaseAdapter extends RecyclerView.Adapter {

    public List<BaseRecyclerViewData> recyclerViewData;
    Context context;

    public BaseAdapter() {

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getViewHolderByType(parent, viewType);
    }

    public abstract RecyclerView.ViewHolder getViewHolderByType(ViewGroup parent, int viewType);


    public <T extends BaseRecyclerViewData> void setData(List<T> data) {
        if (recyclerViewData == null) {
            recyclerViewData = new ArrayList<>();
        }
        recyclerViewData.clear();
        recyclerViewData.addAll(data);
        notifyDataSetChanged();
    }

    public <T extends BaseRecyclerViewData> void addDataList(List<T> data) {
        if (recyclerViewData == null) {
            recyclerViewData = new ArrayList<>();
        }
        int initialSize = recyclerViewData.size();
        recyclerViewData.addAll(data);
        notifyItemRangeInserted(initialSize, data.size());
    }

    public void addSingleData(BaseRecyclerViewData data) {
        if (recyclerViewData == null) {
            recyclerViewData = new ArrayList<>();
        }
        recyclerViewData.add(data);
        notifyItemInserted(recyclerViewData.size() - 1);
    }

    /**
     * Add a BaseRecyclerViewData in recyclerViewData in the specified position
     */
    public void addSingleData(int position, BaseRecyclerViewData data) {
        if (recyclerViewData == null) {
            recyclerViewData = new ArrayList<>();
        }
        recyclerViewData.add(position, data);
        notifyItemInserted(position);
    }

    public void removeItem(BaseRecyclerViewData data) {
        recyclerViewData.remove(data);
        notifyDataSetChanged();
    }

    /**
     * Remove the item at the specified position in recyclerViewData
     */
    public void removeItem(int position) {
        if (recyclerViewData == null) {
            return;
        }
        recyclerViewData.remove(position);
        notifyItemRemoved(position);
    }

    public BaseRecyclerViewData getItemAtPosition(int position) {
        return recyclerViewData.get(position);
    }

    public void clearData() {
        if (recyclerViewData == null) {
            recyclerViewData = new ArrayList<>();
            return;
        }
        recyclerViewData.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (recyclerViewData != null)
            return recyclerViewData.size();
        else return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return recyclerViewData.get(position).getType();
    }

    public Context getCurrentContext() {
        return context;
    }

    public List<BaseRecyclerViewData> getCurrentDataset() {
        if (recyclerViewData != null)
            return recyclerViewData;
        else {
            recyclerViewData = new ArrayList<>();
            return recyclerViewData;
        }
    }

    public void resetData() {
        if (recyclerViewData != null) {
            recyclerViewData.clear();
            notifyDataSetChanged();
        }
    }

    public void updateItemWithType(BaseRecyclerViewData data, int type) {
        int i = 0;
        for (; i < recyclerViewData.size(); i++) {
            if (recyclerViewData.get(i).type == type) {
                recyclerViewData.set(i, data);
                break;
            }
        }
        notifyItemChanged(i);
    }

    protected View inflateViewFromLayout(@LayoutRes int layout, ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
    }
}
