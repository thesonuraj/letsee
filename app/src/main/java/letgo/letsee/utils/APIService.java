package letgo.letsee.utils;

import letgo.letsee.models.config.BaseConfigurations;
import letgo.letsee.models.response.GenreRespose;
import letgo.letsee.models.response.SimilarMovieResponse;
import letgo.letsee.models.response.TopRatedMoviesResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by sonu on 06/01/18.
 */

public interface APIService {
    @GET("movie/top_rated")
    Call<TopRatedMoviesResponse> getTopRatedMovies(
            @Query("language") String language,
            @Query("page") int pageNo,
            @Query("region") String region
    );

    @GET("movie/{movie_id}/similar")
    Call<SimilarMovieResponse> getSimilarMovies(
            @Path("movie_id") int movieID,
            @Query("language") String language,
            @Query("page") int pageNo

    );

    @GET("configuration")
    Call<BaseConfigurations> getConfiguration();

    @GET("genre/movie/list")
    Call<GenreRespose> getGeneres();

}
