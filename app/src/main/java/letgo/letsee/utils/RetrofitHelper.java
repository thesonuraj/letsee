package letgo.letsee.utils;

/**
 * Created by sonu on 06/01/18.
 */

import android.support.annotation.Nullable;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitHelper {

    public static final int TIMEOUT = 30;
    private static Retrofit retrofit = null;
    private static final HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    public static <S> S createService(Class<S> serviceClass) {
        synchronized (RetrofitHelper.class) {
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(getClientBuilder().addInterceptor(logging)
                                .addInterceptor(new Interceptor() {
                                    @Override
                                    public Response intercept(Chain chain) throws IOException {
                                        Request original = chain.request();
                                        HttpUrl originalHttpUrl = original.url();

                                        HttpUrl url = originalHttpUrl.newBuilder()
                                                .addQueryParameter(Constants.API_KEY, Constants.API_KEY_VALUE)
                                                .build();

                                        Request.Builder requestBuilder = original.newBuilder()
                                                .url(url);

                                        Request request = requestBuilder.build();
                                        return chain.proceed(request);
                                    }
                                })
                                .build())
                        .build();
            }
            return retrofit.create(serviceClass);
        }
    }

    @Nullable
    public static OkHttpClient.Builder getClientBuilder() {
        OkHttpClient.Builder Builder = new OkHttpClient.Builder();
        try {
            Builder.connectTimeout(TIMEOUT, TimeUnit.SECONDS);
            Builder.readTimeout(TIMEOUT, TimeUnit.SECONDS);
            Builder.writeTimeout(TIMEOUT, TimeUnit.SECONDS);

            return Builder;
        } catch (Throwable t) {

        }
        return null;
    }


}
