package letgo.letsee.home;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import letgo.letsee.LetSeeApp;
import letgo.letsee.base.BasePresenter;
import letgo.letsee.base.BaseRecyclerViewData;
import letgo.letsee.home.recycler.data.MovieData;
import letgo.letsee.listeners.RetryListener;
import letgo.letsee.models.config.BaseConfigurations;
import letgo.letsee.models.response.GenreRespose;
import letgo.letsee.models.response.Result;
import letgo.letsee.models.response.TopRatedMoviesResponse;
import letgo.letsee.utils.APIService;
import letgo.letsee.utils.Constants;
import letgo.letsee.utils.PreferenceManager;
import letgo.letsee.utils.RetrofitHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sonu on 06/01/18.
 */

public class HomePresenter implements BasePresenter {
    private HomeInterface homeInterface;
    private int page = 0;
    private int totalPages = 0;
    private boolean loading;
    private boolean attached = false;

    HomePresenter(HomeInterface homeInterface) {
        this.homeInterface = homeInterface;
    }

    @Override
    public void onStart(final Bundle bundle) {
        attached = true;
        if (LetSeeApp.getLetSeeApp().isNetworkConnected()) {
            if (!PreferenceManager.getInstance().configurationSaved()) {
                getConfig(bundle);
            } else {
                fetchInitialData();
            }
        } else {
            homeInterface.showNoContent(true);
            homeInterface.onFailure("Seems like you're not connected to internet", new RetryListener() {
                @Override
                public void onRetryClicked() {
                    onStart(bundle);
                }
            });
        }
    }

    private void getConfig(final Bundle bundle) {
        if (attached) {
            homeInterface.showProgress(true);
        }
        RetrofitHelper.createService(APIService.class).getConfiguration().enqueue(new Callback<BaseConfigurations>() {
            @Override
            public void onResponse(Call<BaseConfigurations> call, Response<BaseConfigurations> response) {
                if (attached) {
                    homeInterface.showProgress(false);
                    homeInterface.showNoContent(false);

                }
                if (response.isSuccessful()) {
                    PreferenceManager.getInstance().saveImageConfiguration(response.body().getImages());
                    fetchInitialData();
                } else {
                    onFailure(call, new Throwable(response.message()));
                }
            }

            @Override
            public void onFailure(Call<BaseConfigurations> call, Throwable t) {
                if (attached) {
                    homeInterface.showProgress(false);
                    homeInterface.showNoContent(true);
                    homeInterface.onFailure("Something went wrong!", new RetryListener() {
                        @Override
                        public void onRetryClicked() {
                            onStart(bundle);
                        }
                    });
                }
            }
        });
        RetrofitHelper.createService(APIService.class).getGeneres().enqueue(new Callback<GenreRespose>() {
            @Override
            public void onResponse(Call<GenreRespose> call, Response<GenreRespose> response) {
                if (response.isSuccessful()) {
                    PreferenceManager.getInstance().saveGeneres(response.body().getGenres());
                } else {
                    onFailure(call, new Throwable(response.message()));
                }
            }

            @Override
            public void onFailure(Call<GenreRespose> call, Throwable t) {
            }
        });

    }

    private void fetchInitialData() {
        if (attached) {
            homeInterface.showProgress(true);
        }

        RetrofitHelper.createService(APIService.class).getTopRatedMovies(Constants.LANGUAGE, ++page, null).enqueue(new Callback<TopRatedMoviesResponse>() {
            @Override
            public void onResponse(Call<TopRatedMoviesResponse> call, Response<TopRatedMoviesResponse> response) {
                if (response.isSuccessful()) {
                    if (attached) {
                        homeInterface.onPageChanged(page, totalPages);
                        homeInterface.showProgress(false);
                        homeInterface.showNoContent(false);
                        curateData(response.body(), false);
                    }
                } else {
                    onFailure(call, new Throwable(response.message()));
                }

            }

            @Override
            public void onFailure(Call<TopRatedMoviesResponse> call, Throwable t) {
                page--;
                if (attached) {
                    homeInterface.showProgress(false);
                    homeInterface.showNoContent(true);
                    homeInterface.onFailure(t.getMessage(), new RetryListener() {
                        @Override
                        public void onRetryClicked() {
                            fetchInitialData();
                        }
                    });
                }

            }
        });
    }

    private void curateData(TopRatedMoviesResponse body, boolean add) {
        page = body.getPage();
        totalPages = body.getTotalPages();
        List<BaseRecyclerViewData> recyclerViewDataList = new ArrayList<>();
        for (Result result : body.getResults()) {
            recyclerViewDataList.add(new MovieData(result));
        }


        if (add) {
            homeInterface.onDataLoaded(recyclerViewDataList, body.getPage());
        } else {
            homeInterface.onDataFetched(recyclerViewDataList);
        }

    }

    boolean shouldLoadMore() {
        return totalPages > page;
    }

    void loadMore() {
        if (!loading) {
            loading = true;
            if (attached) {
                homeInterface.showRVProgress(true);
            }
            RetrofitHelper.createService(APIService.class).getTopRatedMovies("en-US", ++page, null).enqueue(new Callback<TopRatedMoviesResponse>() {
                @Override
                public void onResponse(Call<TopRatedMoviesResponse> call, Response<TopRatedMoviesResponse> response) {
                    if (response.isSuccessful()) {
                        loading = false;
                        if (attached) {
                            homeInterface.showRVProgress(false);
                            homeInterface.onPageChanged(page, totalPages);
                            homeInterface.showNoContent(false);
                            curateData(response.body(), true);
                        }

                    } else {
                        onFailure(call, new Throwable(response.message()));
                    }

                }

                @Override
                public void onFailure(Call<TopRatedMoviesResponse> call, Throwable t) {
                    page--;
                    if (attached) {
                        homeInterface.showRVProgress(false);
                    }
                    homeInterface.showNoContent(true);
                    homeInterface.onFailure("Something went wrong!", new RetryListener() {
                        @Override
                        public void onRetryClicked() {
                            fetchInitialData();
                        }
                    });
                    loading = false;

                }
            });
        }
    }

    @Override
    public void onResume() {
        attached = true;
    }

    @Override
    public void onDestroy() {
        attached = false;
    }

    @Override
    public void onPause() {

    }


    public void fetchNext() {
        if (page < totalPages) {
            fetchInitialData();
        }
    }

    public void fetchFirst() {
        page = 0;
        fetchInitialData();

    }

    public void fetchPrevious() {
        if (page > 1) {
            page = page - 2;
            fetchInitialData();
        }

    }

    public void fetchLast() {
        page = totalPages - 1;
        fetchInitialData();
    }

    public void loadPage(int page) {
        this.page = page - 1;
        fetchInitialData();
    }
}
