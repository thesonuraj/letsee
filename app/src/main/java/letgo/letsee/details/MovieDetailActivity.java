package letgo.letsee.details;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import letgo.letsee.R;
import letgo.letsee.base.BaseActivity;
import letgo.letsee.base.BaseRecyclerViewData;
import letgo.letsee.databinding.ActivityMovieDetailBinding;
import letgo.letsee.details.recycler.SimilarAdapter;
import letgo.letsee.home.recycler.data.MovieData;
import letgo.letsee.utils.Constants;

public class MovieDetailActivity extends BaseActivity implements MovieDetailInterface {

    MovieDetailPresenter movieDetailPresenter;
    private ActivityMovieDetailBinding activityMovieDetailBinding;
    SimilarAdapter similarAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        activityMovieDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail);
        movieDetailPresenter = new MovieDetailPresenter(this);
        init(movieDetailPresenter, findViewById(R.id.no_layout), findViewById(R.id.root));
        initSimilar();
        movieDetailPresenter.onStart(getIntent().getExtras());
    }

    private void initSimilar() {
        similarAdapter = new SimilarAdapter(this);
        similarAdapter.setData(new ArrayList<BaseRecyclerViewData>());
        activityMovieDetailBinding.similarList.setAdapter(similarAdapter);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        activityMovieDetailBinding.similarList.setLayoutManager(linearLayoutManager);
        activityMovieDetailBinding.similarList.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItem = linearLayoutManager.getItemCount();
                int getLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (getLastVisibleItem == totalItem - 1 && movieDetailPresenter.shouldLoadMore()) {
                    movieDetailPresenter.loadMore();
                }

            }
        });
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void setData(MovieData movieData) {
        activityMovieDetailBinding.setData(movieData);
    }

    @Override
    public void onDataFetched(List<BaseRecyclerViewData> recyclerViewData) {
        similarAdapter.setData(recyclerViewData);

    }

    @Override
    public void onDataLoaded(List<BaseRecyclerViewData> recyclerViewData, int page) {
        similarAdapter.addDataList(recyclerViewData);
    }

    @Override
    public void onItemClicked(MovieData item) {
        Intent intent = new Intent(this, MovieDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.DATA, item);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void showRVProgress(boolean show) {
        if (show) {
            BaseRecyclerViewData baseRecyclerViewData = new BaseRecyclerViewData(BaseRecyclerViewData.ITEM_TYPE_PROGRESS);
            if (similarAdapter.getCurrentDataset().size() == 0 || similarAdapter.getCurrentDataset().get(similarAdapter.getCurrentDataset().size() - 1).getType() != BaseRecyclerViewData.ITEM_TYPE_PROGRESS) {
                similarAdapter.addSingleData(baseRecyclerViewData);
            }
        } else {
            similarAdapter.removeItem(similarAdapter.getCurrentDataset().size() - 1);
        }
    }

    @Override
    public void onPageChanged(int page, int totalPage) {

    }

}
