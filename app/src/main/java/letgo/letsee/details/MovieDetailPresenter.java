package letgo.letsee.details;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import letgo.letsee.base.BasePresenter;
import letgo.letsee.base.BaseRecyclerViewData;
import letgo.letsee.home.recycler.data.MovieData;
import letgo.letsee.models.response.Result;
import letgo.letsee.models.response.SimilarMovieResponse;
import letgo.letsee.utils.APIService;
import letgo.letsee.utils.Constants;
import letgo.letsee.utils.RetrofitHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sonu on 08/01/18.
 */

public class MovieDetailPresenter implements BasePresenter {

    MovieDetailInterface movieDetailInterface;
    MovieData movieData;
    int pageNo = 0;
    private int totalPages;
    private boolean attached = false;
    private boolean loading;

    public MovieDetailPresenter(MovieDetailInterface movieDetailInterface) {
        this.movieDetailInterface = movieDetailInterface;
    }

    @Override
    public void onStart(Bundle bundle) {
        attached = true;
        movieData = (MovieData) bundle.getSerializable(Constants.DATA);
        movieDetailInterface.setData(movieData);
        fetchSimilarVideos(++pageNo);

    }

    public void fetchSimilarVideos(final int pageNo) {
        if (!loading) {
            loading = true;
            if (attached) {
                movieDetailInterface.showRVProgress(true);
            }
            RetrofitHelper.createService(APIService.class).getSimilarMovies(movieData.getId(), Constants.LANGUAGE, pageNo).enqueue(new Callback<SimilarMovieResponse>() {
                @Override
                public void onResponse(Call<SimilarMovieResponse> call, Response<SimilarMovieResponse> response) {
                    loading = false;
                    if (response.isSuccessful()) {
                        if (attached) {
                            movieDetailInterface.showRVProgress(false);
                            movieDetailInterface.onPageChanged(pageNo,totalPages);
                            curateData(response.body(), pageNo > 1);
                        }
                    } else {
                        onFailure(call, new Throwable(response.message()));
                    }
                }

                @Override
                public void onFailure(Call<SimilarMovieResponse> call, Throwable t) {
                    loading = false;

                    if (attached) {
                        movieDetailInterface.showRVProgress(false);
                    }

                }
            });
        }
    }

    private void curateData(SimilarMovieResponse body, boolean add) {
        pageNo = body.getPage();
        totalPages = body.getTotalPages();
        List<BaseRecyclerViewData> recyclerViewDataList = new ArrayList<>();
        for (Result result : body.getResults()) {
            recyclerViewDataList.add(new MovieData(result));
        }


        if (add) {
            movieDetailInterface.onDataLoaded(recyclerViewDataList, body.getPage());
        } else {
            recyclerViewDataList.add(0, movieData);
            movieDetailInterface.onDataFetched(recyclerViewDataList);
        }

        Log.e("Datafetched", "size : " + body.getResults().size());
    }


    @Override
    public void onResume() {
        attached = true;
    }

    @Override
    public void onDestroy() {
        attached = false;
    }

    @Override
    public void onPause() {

    }

    boolean shouldLoadMore() {
        return totalPages > pageNo;
    }

    public void loadMore() {
        fetchSimilarVideos(++pageNo);
    }
}
