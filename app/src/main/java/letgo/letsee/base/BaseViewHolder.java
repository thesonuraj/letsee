package letgo.letsee.base;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import letgo.letsee.R;


/**
 * Created by sonu on 06/01/18.
 */

public class BaseViewHolder {
    public ProgressBar progressBar;
    public RelativeLayout noContentView;
    public TextView retry;
    public TextView wrongText;


    public BaseViewHolder(View view) {
        this.progressBar = view.findViewById(R.id.progress);
        this.noContentView = view.findViewById(R.id.no_content_layout);
        this.retry = view.findViewById(R.id.retry);
        this.wrongText = view.findViewById(R.id.wrong_text);
    }
}
